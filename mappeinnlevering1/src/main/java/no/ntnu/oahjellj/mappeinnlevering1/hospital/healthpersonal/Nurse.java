package no.ntnu.oahjellj.mappeinnlevering1.hospital.healthpersonal;

import no.ntnu.oahjellj.mappeinnlevering1.hospital.Employee;

/**
 * Nurse is a subclass of employee with the title nurse
 * 
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 * @see Employee
 */
public class Nurse extends Employee {
    /**
     * Constructor from person (super)
     * @param firstName : String
     * @param lastName : String
     * @param socialSecurityNumber : String
     * @see Person
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    
    /** 
     * Makes a string of the person (super) with the title nurse.
     * @return String
     * @see Person 
     */
    public String toString() {
        return "Sykepleier: " + super.toString();
    }
    
}

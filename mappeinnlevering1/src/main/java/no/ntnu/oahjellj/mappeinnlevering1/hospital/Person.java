package no.ntnu.oahjellj.mappeinnlevering1.hospital;

/**
 * Person is the super class for the Patient class and Employee classes, 
 * and represents a person with a name and social security number.
 * 
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 */
public class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;


    /**
     * Constructor
     * @param firstName : String
     * @param lastName : String
     * @param socialSecurityNumber : String
     */
    protected Person(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    
    /** 
     * Gets first name
     * @return String
     */
    protected String getFirstName() {
        return this.firstName;
    }

    
    /** 
     * Gets last name
     * @return String
     */
    protected String getLastName() {
        return this.lastName;
    }


    /**
     * Gets name
     * @return String
     */
    public String getName() {
        return firstName + " " + lastName;
    }

    
    /** 
     * Gets social security number
     * @return String
     */
    protected String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }

    
    /** 
     * Makes a string representing the person (Super)
     * @return String
     */
    public String toString() {
        return getName();
    }

    
    /** 
     * Checks the given object is equal this
     * @param obj : Object
     * @return boolean
     */
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Person)) return false;
        return this.socialSecurityNumber.equals(((Person) obj).getSocialSecurityNumber());
    }

}

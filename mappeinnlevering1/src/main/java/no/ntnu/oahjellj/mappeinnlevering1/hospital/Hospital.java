package no.ntnu.oahjellj.mappeinnlevering1.hospital;

import java.util.HashMap;

/**
* The hospital class keeps the name and list of departments for a hospital.
*
* @author  Alida Hjelljord
* @version 1.0
* @since   10-03-2021 
*/
public class Hospital {
    private final String hospitalName;
    private HashMap <String, Department> departments;

    /**
     * Constructor
     * @param hospitalName : String
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new HashMap<>();
    }

    
    /** 
     * Gets the name of the hospital
     * @return String
     */
    public String getHospitalName() {
        return hospitalName;
    }

    
    /** 
     * Gets the list of departments
     * @return HashMap<String, Department>
     */
    public HashMap<String, Department> getDepartments() {
        return departments;
    }

    
    /** 
     * Adds the given department to the list of departments
     * @param department : Department
     * @return true if the new department is added succesfully
     */
    public boolean addDepartment(Department department) {
        if(departments.keySet().contains(department.getDepartmentName())){
            departments.put(department.getDepartmentName(), department);
            return true;
        }
        return false;
    }

    
    /** 
     * Makes a string on the form: <pre>
     * 
     * hospitalName
     * 
     *-list of departments- </pre>
     * @return String
     */
    public String toString() {
        String s = hospitalName + "\n\n";
        for(String key : departments.keySet()) {
            s += departments.get(key).toString() + "\n";
        }
        return s;
    }
}

package no.ntnu.oahjellj.mappeinnlevering1.hospital;

/**
 * Interface, primerly for the Patient class, that add the method setDiagnisis.
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 * @see Patient
 */
public interface Diagnosiable {
    public void setDiagnosis(String diagnosis);
}

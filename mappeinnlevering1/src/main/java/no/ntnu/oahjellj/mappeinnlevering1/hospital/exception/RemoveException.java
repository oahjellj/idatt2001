package no.ntnu.oahjellj.mappeinnlevering1.hospital.exception;

/**
 * Custom exception for the remove method in Department
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 * @see Department
 */
public class RemoveException extends Exception{
    private static final long serialVersionUID = 1L;

    public RemoveException(String errorString) {
        super(errorString);
    }

    public RemoveException(String string, NullPointerException e) {
    }
}

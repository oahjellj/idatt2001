package no.ntnu.oahjellj.mappeinnlevering1.hospital;

/**
 * Patient is a subclass of person that implements Diagnosable to make doctors able to diagnose patients.
 * 
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 * @see Person
 * @see Diagnosiable
 */
public class Patient extends Person implements Diagnosiable {

    private String diagnosis;

    /**
     * Constructs a Patient with diagnosis set to "Ikke diagnosert."
     * @param firstName : String
     * @param lastName : String
     * @param socialSecurityNumber : String
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
        this.diagnosis = "Ikke diagnosert.";
    }

    
    /** 
     * Sets the diagnosis, accesable for doctor
     * @param diagnosis : String
     * @see Doctor
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    
    
    /** 
     * Gets diagnosis
     * @return String
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    
    /** 
     * Makes a string from super with the diagnosis
     * @return String
     */
    public String toString() {
        return super.toString() + " Diagnose: " + diagnosis;
    }
}

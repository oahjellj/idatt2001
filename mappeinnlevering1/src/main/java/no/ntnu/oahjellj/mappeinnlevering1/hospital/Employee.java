package no.ntnu.oahjellj.mappeinnlevering1.hospital;

/**
 * Employee is a subclass of person
 * 
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 * @see Person
 */
public class Employee extends Person {
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}

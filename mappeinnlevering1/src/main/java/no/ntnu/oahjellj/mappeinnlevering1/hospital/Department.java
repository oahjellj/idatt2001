package no.ntnu.oahjellj.mappeinnlevering1.hospital;

import java.util.ArrayList;
import no.ntnu.oahjellj.mappeinnlevering1.hospital.exception.RemoveException;

/**
* The department class keeps lists of patients and employees for a department of a hospital. 
*
* @author  Alida Hjelljord
* @version 1.0
* @since   10-03-2021 
*/
public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Constructs a new department with the given name
     * @param departmentName : String
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    
    /** 
     * Gets the name of the department
     * @return String
     */
    public String getDepartmentName() {
        return this.departmentName;
    }

    
    /** 
     * Sets the name of the department
     * @param departmentName : String
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    
    /** 
     * Gets the list of employees
     * @return ArrayList<Employee>
     */
    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    
    /** 
     * Adds the given employee to the list of employees
     * @param employee : Employee
     * @return true if emplyee succesfully added to the list
     */
    public boolean addEmployee(Employee employee) {
        if(!employees.contains(employee)) {
            return employees.add(employee);
        }
        return false;
    }

    
    /** 
     * Gets the list of employees
     * @return ArrayList<Patient>
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    
    /** 
     * Adds the given patient to the list of patients.
     * @param patient : Patient
     * @return true if patient succesfully added
     */
    public boolean addPatient(Patient patient) {
        if(!patients.contains(patient)) {
            return patients.add(patient);
        }
        return false;
    }

    
    /** 
     * Makes a hash based on the department name
     * @return int
     */
    public int hashCode() {
        return departmentName.hashCode();
    }

    
    /** 
     * Removes a person from the a list they're in, if the given person is not in any of the list a RemoveException is thrown.
     * @param person : Person or any subclass
     * @throws RemoveException
     */
    public void remove(Person person) throws RemoveException {
        if(!employees.remove(person) && !patients.remove(person)) throw new RemoveException("Person not found.");
    }
    
    /** 
     * Checks if to departments are equal given department name
     * @param obj : Object
     * @return true if the given object is equal to this department
     */
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Department)) return false;
        return departmentName.equals(((Department) obj).getDepartmentName());
    }

    
    /** 
     * Makes a string on the form:<pre>
     * departmentName
     * 
     * Employees:
     * -list-
     * 
     * Patients:
     * -list-
     * </pre>
     * @return String
     */
    public String toString() {
        String s = departmentName + "\n\n" + "Employees:\n";
        for(Employee employee : employees) {
            s += employee.toString() + "\n";
        }
        s += "\nPatients:\n";
        for(Patient patient : patients) {
            s += patient.toString() + "\n";
        }
        return s;
    }
}

package no.ntnu.oahjellj.mappeinnlevering1.hospital.healthpersonal;

import no.ntnu.oahjellj.mappeinnlevering1.hospital.Employee;
import no.ntnu.oahjellj.mappeinnlevering1.hospital.Patient;

/**
 * Abstract class outlining doctors, a subclass of Employee that can set the diagnosis of patients.
 *
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021 
 * @see Employee
 */
public abstract class Doctor extends Employee {
    /**
     * Constructor from person (super)
     * @param firstName : String
     * @param lastName : String
     * @param socialSecurityNumber : String
     * @see Person
     */
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    
    /**
     * Abstract method for setting diagnosees
     * @param patient : Patient
     * @param diagnosis : String
     * @see Patient
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}

package no.ntnu.oahjellj.mappeinnlevering1.hospital.healthpersonal;

import no.ntnu.oahjellj.mappeinnlevering1.hospital.Patient;

/**
 * GreneralPractitioner is a subclass of Doctor
 *
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 * @see Doctor 
 */
public class Surgeon extends Doctor {
    /**
     * Constructor from person (super)
     * @param firstName : String
     * @param lastName : String
     * @param socialSecurityNumber : String
     * @see Person
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    
    /** 
     * Sets the diagnosis of the given patient 
     * @param patient
     * @param diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    
    /** 
     * Makes a string of the person (super) with the title "Kirurg"
     * @return String
     * @see Person 
     */
    public String toString() {
        return "Kirurg: " + super.toString();
    }
    
}

package no.ntnu.oahjellj.mappeinnlevering1.hospital.testClient;

import no.ntnu.oahjellj.mappeinnlevering1.hospital.*;
import no.ntnu.oahjellj.mappeinnlevering1.hospital.exception.RemoveException;

public class HospitalTestClient {
    
    public static void main(String[] args) throws RemoveException {
        Hospital hospital =  new Hospital("St. Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);
        Department department = hospital.getDepartments().get("Akutten");

        // removes first employe in the department, whats printed should not iclude "Odd Even Primtallet"
        department.remove(new Employee("Odd Even", "Primtallet", ""));
        
        System.out.println(department.toString());

        // tries to remove inexitant patient shoud print "Person not found."
        try {
            department.remove(new Patient("Noldus", "Noldus", "Ikke oppgitt"));
        } catch (RemoveException e) {
            System.out.println("Person not found.");
        }
    }
}

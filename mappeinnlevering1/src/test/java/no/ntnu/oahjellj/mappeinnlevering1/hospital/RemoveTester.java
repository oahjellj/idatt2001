package no.ntnu.oahjellj.mappeinnlevering1.hospital;

import org.junit.Test;
import junit.framework.TestCase;
import no.ntnu.oahjellj.mappeinnlevering1.hospital.exception.RemoveException;
import no.ntnu.oahjellj.mappeinnlevering1.hospital.healthpersonal.*;

/**
 * JUnitTest for remove(Person person) from Department 
 * 
 * @author  Alida Hjelljord
 * @version 1.0
 * @since   10-03-2021
 * @see Department
 */
public class RemoveTester extends TestCase{
    
    /** 
     * Positive test for remove
     */
    @Test
    public void testRemovePositive() {
        //set up department with employee
        Department department = new Department("Radium Department");
        String desiredResult = department.toString();
        Nurse nurse = new Nurse("Emma", "Hansen", "230497 43523");
        department.addEmployee(nurse);

        //remove the given employee
        try {
            department.remove(nurse);
        } catch (RemoveException e) {
            fail();
        }
        
        assertEquals(desiredResult, department.toString());
    }

    
    /** 
     * Negative test for remove
     * @throws RemoveException
     */
    @Test
    public void testRemoveNegative() {
        //set up department with employee
        Department department = new Department("Radium Department");
        Nurse nurse = new Nurse("Emma", "Hansen", "230497 43523");
        department.addEmployee(nurse);

        //try and remove a non-extistent employee
        try {
            department.remove(new GeneralPractitioner("Hans", "Emmerson", "300695 84932"));
            fail("No error occured.");
        } catch (RemoveException e) {
            return;
        }
    }
}
